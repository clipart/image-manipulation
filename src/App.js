import React from 'react';
import DropArea from './components/DropArea';
import ImagesList from './components/ImagesList';
import WorkArea from './components/WorkArea';
import uuidv4 from "@bundled-es-modules/uuid/v4.js";
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {loadedImages: [], boardImages: []};
    this.handleDrop = this.handleDrop.bind(this);
    this.handleImageSelected = this.handleImageSelected.bind(this);
    this.handleImageMove = this.handleImageMove.bind(this);
  }

  handleDrop(files) {
    const images = files.filter((file) => {
      const ext = file.name.split(".").pop().toLowerCase();

      return ["jpg", "jpeg", "png"].find((e) => e === ext);
    });

    if (images.length === 0) {
      console.warn("No supported images found.");

      return;
    }

    const { loadedImages } = this.state;
    const updatedImages =  loadedImages.concat(images.map(file => {
      const fileId = uuidv4();
      const reader = new FileReader();

      reader.onabort = () => console.error(`file ${fileId} reading was aborted`)
      reader.onerror = () => console.error(`file ${fileId} reading has failed`)
      reader.onload = () => {
        this.onFileLoaded(fileId, reader.result);
      }

      reader.readAsDataURL(file);

       return {
          id: fileId,
          fileName: file.name,
          isLoading: true
        };
    }));

    this.setState(state => Object.assign(state, { loadedImages: updatedImages }));
  }

  onFileLoaded(id, dataURL) {
    const { loadedImages } = this.state;
    const imageIndex = loadedImages.findIndex((img) => img.id === id);

    if (imageIndex === -1) {
      console.error(`Image ${id} not found.`);

      return;
    }

    const undatedImage = Object.assign(loadedImages[imageIndex], { isLoading: false, dataURL: dataURL });
    const updatedImages = [...(loadedImages.slice(0, imageIndex)), undatedImage].concat(loadedImages.slice(imageIndex + 1));

    this.setState(state => Object.assign(state, { loadedImages: updatedImages }));
  }

  handleImageSelected(id) {
    const { loadedImages } = this.state;
    const image = loadedImages.find((img) => img.id === id);

    if (!image) {
      console.error(`Image ${id} not found.`);

      return;
    }

    this.setState(state => Object.assign(state, { boardImages: [...state.boardImages, { id: uuidv4(), dataURL: image.dataURL, x: 0, y: 0 }] }));
  }

  handleImageMove(id, dx, dy) {
    const { boardImages } = this.state;
    const imageIndex = boardImages.findIndex((img) => img.id === id);

    if (imageIndex === -1) {
      console.error(`Image ${id} not found.`);

      return;
    }

    const undatedImage = Object.assign(boardImages[imageIndex], { x: boardImages[imageIndex].x + dx, y: boardImages[imageIndex].y + dy });
    const updatedImages = [...(boardImages.slice(0, imageIndex)), undatedImage].concat(boardImages.slice(imageIndex + 1));

    this.setState(state => Object.assign(state, { boardImages: updatedImages }));
  }

  render() {

    return (
      <div className="App">
        <div className="row">
          <div className="col">
            <DropArea onDrop={this.handleDrop} />
            <ImagesList images={this.state.loadedImages} onSelected={this.handleImageSelected} />
          </div>
          <div className="col">
            <WorkArea boardImages={this.state.boardImages} onImageMove={this.handleImageMove} />
          </div>
        </div>

      </div>
    );
  }
}

export default App;
