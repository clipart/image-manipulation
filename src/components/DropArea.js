import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import './DropArea.css';

function DropArea(props) {
    const { onDrop } = props;

    return (
        <Dropzone onDrop={onDrop}>
            {({ getRootProps, getInputProps }) => (
                <div {...getRootProps({ className: 'dropzone' })}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop some images here, or click to select files</p>
                </div>
            )}
        </Dropzone>
    );
}

DropArea.propTypes = {
    onDrop: PropTypes.func
};

export default DropArea;