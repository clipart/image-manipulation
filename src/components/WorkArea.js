import React from 'react';
import PropTypes from 'prop-types';
import './WorkArea.css';

class WorkArea extends React.Component {
    static FPS = 60;

    _isMoving;
    _cursorX;
    _cursorY;
    _prevX;
    _prevY;
    _moveImageId;
    _timer;
    _isTimerBusy;

    constructor(props) {
        super(props);

        this._isMoving = false;
        this._moveImageId = null;
        this._cursorX = 0;
        this._cursorY = 0;
        this._prevX = 0;
        this._prevY = 0;
        this._timer = null;
        this._isTimerBusy = false;

        this.state = {};
        this.handleBeginMove = this.handleBeginMove.bind(this);
        this.handleMove = this.handleMove.bind(this);
        this.handleEndMove = this.handleEndMove.bind(this);
    }

    handleBeginMove(event, id) {
        this._isMoving = true;
        this._moveImageId = id;
        this._cursorX = event.pageX;
        this._cursorY = event.pageY;
        this._prevX = this._cursorX;
        this._prevY = this._cursorY;

        this._timer = setInterval(() => {
            if (this._isTimerBusy) {
                return;
            }

            this._isTimerBusy = true;

            const { onImageMove } = this.props;
            const dx = this._cursorX - this._prevX;
            const dy = this._cursorY - this._prevY;

            if (dx !== 0 || dy !== 0) {
                onImageMove(this._moveImageId, dx, dy);

                this._prevX = this._cursorX;
                this._prevY = this._cursorY;
            }

            this._isTimerBusy = false;
        }, 1000 / WorkArea.FPS);
    }

    handleMove(event) {
        if (!this._isMoving) {
            return;
        }

        this._cursorX = event.pageX;
        this._cursorY = event.pageY;
    }

    handleEndMove() {
        this._isMoving = false;
        this._moveImageId = null;
        if (this._timer) {
            clearInterval(this._timer);
            this._timer = null;
        }
    }

    render() {
        const { boardImages } = this.props;

        const images = Array.isArray(boardImages) && boardImages.length > 0
            ? boardImages.map((image) => {
                const imagesStyle = {
                    transform: `translate3d(${image.x}px, ${image.y}px, 0)`
                };

                return (
                    <div key={image.id}
                        className="image-container"
                        style={imagesStyle}
                        onMouseDown={(e) => this.handleBeginMove(e, image.id)}
                        onMouseUp={this.handleEndMove}>
                        <img src={image.dataURL} draggable="false"></img>
                    </div>
                );
            })
            : "";

        return (
            <div className="work-area" onMouseMove={this.handleMove} onMouseLeave={this.handleEndMove}>
                {images}
            </div>
        );
    }
}

WorkArea.propTypes = {
    boardImages: PropTypes.array,
    onImageMove: PropTypes.func
};

export default WorkArea;