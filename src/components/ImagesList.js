import React from 'react';
import PropTypes from 'prop-types';
import './ImagesList.css';

function ImagesList(props) {
  const { images, onSelected } = props;

  const imagesList = !Array.isArray(images)
    ? ""
    : images.map((image) => {
      let classes = "image-list-item";
      if (image.isLoading) {
        classes += " loading";
      }

      return (
        <li key={image.id} className={classes} onClick={() => onSelected(image.id)}>{image.fileName}</li>
      );
    });

  return (
    <div>
      <h4>Images:</h4>
      <ul>{imagesList}</ul>
    </div>
  );
}

ImagesList.propTypes = {
  images: PropTypes.array,
  onSelected: PropTypes.func
};

export default ImagesList;